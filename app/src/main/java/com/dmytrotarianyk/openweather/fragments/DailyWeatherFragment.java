package com.dmytrotarianyk.openweather.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.dmytrotarianyk.openweather.DayForecastDialog;
import com.dmytrotarianyk.openweather.HourForecastDialog;
import com.dmytrotarianyk.openweather.MainActivity;
import com.dmytrotarianyk.openweather.OpenWeatherApp;
import com.dmytrotarianyk.openweather.R;
import com.dmytrotarianyk.openweather.Util;
import com.dmytrotarianyk.openweather.api.ApiConstants;
import com.dmytrotarianyk.openweather.api.OpenWeatherMapApi;
import com.dmytrotarianyk.openweather.models.DayForecast;
import com.dmytrotarianyk.openweather.models.DayForecastCollection;
import com.dmytrotarianyk.openweather.models.HourForecast;
import com.dmytrotarianyk.openweather.models.LocationInfo;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DailyWeatherFragment extends Fragment {

    private static final String LONGITUDE = "longitude";
    private static final String LATITUDE = "latitude";
    private static final String LOCATION_NAME = "location_name";
    private static final int REQUEST_CONFIRM = 5;

    private View mView;
    private MainActivity mActivity;
    private RequestQueue mRequestQueue = OpenWeatherApp.getRequestQueue();
    private ImageLoader mImageLoader = OpenWeatherApp.getImageLoader();
    private double mLongitude;
    private double mLatitude;
    private String mLocationName;
    private DayForecastCollection mForecastCollection;
    private SwipeRefreshLayout mSwipeLayout;

    public static DailyWeatherFragment newInstance(LocationInfo locationInfo) {
        DailyWeatherFragment f = new DailyWeatherFragment();
        if (locationInfo != null) {
            Bundle args = new Bundle();
            args.putDouble(LONGITUDE, locationInfo.getLongitude());
            args.putDouble(LATITUDE, locationInfo.getLatitude());
            args.putString(LOCATION_NAME, locationInfo.getDisplayName());
            f.setArguments(args);
        }
        return f;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (MainActivity) activity;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        if (args != null) {
            mLongitude = args.getDouble(LONGITUDE);
            mLatitude = args.getDouble(LATITUDE);
            mLocationName = args.getString(LOCATION_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mView = inflater.inflate(R.layout.daily_weather_fragment, container, false);
        mSwipeLayout = (SwipeRefreshLayout) mView.findViewById(R.id.swipe_container);
        return mView;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Resources res = getResources();
        mSwipeLayout.setOnRefreshListener(refreshListener);
        mSwipeLayout.setColorSchemeColors(res.getColor(R.color.pull_to_refresh_1), res.getColor(R.color.pull_to_refresh_2),
                res.getColor(R.color.pull_to_refresh_3), res.getColor(R.color.pull_to_refresh_4));

        loadData();
    }

    private void loadData() {
        mRequestQueue.add(OpenWeatherMapApi.getDailyForecast(mLocationName,
                new Response.Listener<DayForecastCollection>() {
                    @Override
                    public void onResponse(DayForecastCollection response) {
                        if (response != null) {
                            mForecastCollection = response;
                            updateUi();
                        }

                        mActivity.setSupportProgressBarIndeterminateVisibility(false);
                        mSwipeLayout.setRefreshing(false);
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        mActivity.setSupportProgressBarIndeterminateVisibility(false);
                        mSwipeLayout.setRefreshing(false);
                        // TODO: handle errors
                    }
                }));
    }

    private void updateUi() {
        if (getActivity() == null) return;

        DailyWeatherAdapter weatherAdapter = new DailyWeatherAdapter();

        ListView listView = (ListView) mView.findViewById(R.id.list);
        if (listView.getAdapter() == null) {
            listView.setAdapter(weatherAdapter);
        } else {
            weatherAdapter.notifyDataSetChanged();
        }

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                DayForecast forecast = (DayForecast) adapterView.getItemAtPosition(position);
                DayForecastDialog dialog = new DayForecastDialog(getActivity(), forecast);
                dialog.show();
            }
        });

        hideLoadingIndicator();
    }

    private void hideLoadingIndicator() {
        mView.findViewById(R.id.list).setVisibility(View.VISIBLE);
        mView.findViewById(R.id.loading_indicator).setVisibility(View.GONE);
    }

    public void showRefreshConfirmDialog() {
        ConfirmDialogFragment confirmFragment = new ConfirmDialogFragment();
        confirmFragment.setTargetFragment(this, REQUEST_CONFIRM);
        getActivity().getSupportFragmentManager().beginTransaction()
                .add(confirmFragment, "confirm_dialog").commit();
    }

    private void refresh(boolean showIndeterminateProgress) {
        mActivity.setSupportProgressBarIndeterminateVisibility(showIndeterminateProgress);
        loadData();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CONFIRM && resultCode == Activity.RESULT_OK) {
            refresh(true);
        }
    }

    SwipeRefreshLayout.OnRefreshListener refreshListener = new SwipeRefreshLayout.OnRefreshListener() {
        @Override
        public void onRefresh() {
            mSwipeLayout.setRefreshing(true);
            refresh(false);
        }
    };

    private class DailyWeatherAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return mForecastCollection.size();
        }

        @Override
        public Object getItem(int position) {
            return mForecastCollection.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            ViewHolder vh;

            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity())
                        .inflate(R.layout.list_item_day_forecast, parent, false);

                vh = new ViewHolder();
                vh.day = (TextView) convertView.findViewById(R.id.day);
                vh.date = (TextView) convertView.findViewById(R.id.date);
                vh.weatherIcon = (NetworkImageView) convertView.findViewById(R.id.weather_icon);
                vh.tempH = (TextView) convertView.findViewById(R.id.temp_h);
                vh.tempL = (TextView) convertView.findViewById(R.id.temp_l);
                vh.conditions = (TextView) convertView.findViewById(R.id.conditions);
                vh.humidity = (TextView) convertView.findViewById(R.id.humidity);
                vh.windSpeed = (TextView) convertView.findViewById(R.id.wind_speed);

                convertView.setTag(vh);
            } else {
                vh = (ViewHolder) convertView.getTag();
            }

            DayForecast forecast = (DayForecast) getItem(position);
            vh.weatherIcon.setImageUrl(String.format(ApiConstants.WEATHER_ICON_URL, forecast.getIcon()), mImageLoader);
            vh.day.setText(Util.getWeekDay("EEE", forecast.getDt()));
            vh.date.setText(Util.getDate(forecast.getDt()));
            vh.tempH.setText(getString(R.string.temp_high_fmt, forecast.getTempH()) + "\u00B0" + "C");
            vh.tempL.setText(getString(R.string.temp_low_fmt, forecast.getTempL()) + "\u00B0" + "C");
            vh.conditions.setText(forecast.getConditions());
            vh.humidity.setText(forecast.getHumidity() + "%");
            String wind = String.format(getString(R.string.wind_fmt), forecast.getWindSpeed());
            vh.windSpeed.setText(wind + " m/s");

            return convertView;
        }
    }

    static class ViewHolder {
        public TextView day;
        public TextView date;
        public NetworkImageView weatherIcon;
        public TextView tempH;
        public TextView tempL;
        public TextView conditions;
        public TextView humidity;
        public TextView windSpeed;
    }
}
