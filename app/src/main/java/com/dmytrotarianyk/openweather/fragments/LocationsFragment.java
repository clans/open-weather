package com.dmytrotarianyk.openweather.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.dmytrotarianyk.openweather.MainActivity;
import com.dmytrotarianyk.openweather.PreferenceHelper;
import com.dmytrotarianyk.openweather.R;
import com.dmytrotarianyk.openweather.api.OpenWeatherMapApi;
import com.dmytrotarianyk.openweather.models.LocationInfo;

import java.util.ArrayList;
import java.util.List;

public class LocationsFragment extends DialogFragment {

    // Time between search queries while typing.
    private static final int QUERY_DELAY_MILLIS = 500;

    private ListView mSearchResultsList;
    private SearchResultsListAdapter mSearchResultsAdapter;
    private String mQuery;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        View view = inflater.inflate(R.layout.locations_fragment, null);
        EditText mSearchView = (EditText) view.findViewById(R.id.find_city);
        mSearchView.addTextChangedListener(textWatcher);

        mSearchResultsList = (ListView) view.findViewById(R.id.list);
        mSearchResultsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> listView, View view, int position, long itemId) {
                LocationInfo location = (LocationInfo) mSearchResultsAdapter.getItem(position);

                if (location == null) {
                    PreferenceHelper.clearSelectedLocation();
                } else {
                    PreferenceHelper.setLongitude(location.getLongitude());
                    PreferenceHelper.setLatitude(location.getLatitude());
                    PreferenceHelper.setLocationName(location.getDisplayName());
                }

                ((MainActivity) getActivity()).updateLocation();

                dismiss();
            }
        });

        tryBindList();

        AlertDialog dialog = new AlertDialog.Builder(getActivity()).setView(view).create();
        dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        return dialog;
    }

    private void tryBindList() {
        if (isAdded() && mSearchResultsAdapter == null) {
            mSearchResultsAdapter = new SearchResultsListAdapter();
        }

        if (mSearchResultsAdapter != null) {
            if (mSearchResultsList != null) {
                mSearchResultsList.setAdapter(mSearchResultsAdapter);
            }
        }
    }

    private TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i2, int i3) {
        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i2, int i3) {
            mQuery = charSequence.toString();
            if (mRestartLoaderHandler.hasMessages(0)) {
                return;
            }

            mRestartLoaderHandler.sendMessageDelayed(
                    mRestartLoaderHandler.obtainMessage(0),
                    QUERY_DELAY_MILLIS);
        }

        @Override
        public void afterTextChanged(Editable editable) {
        }
    };

    private Handler mRestartLoaderHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            Bundle args = new Bundle();
            args.putString("query", mQuery);
            getLoaderManager().restartLoader(0, args, loaderCallbacks);
        }
    };

    private LoaderManager.LoaderCallbacks<List<LocationInfo>> loaderCallbacks =
            new LoaderManager.LoaderCallbacks<List<LocationInfo>>() {
                @Override
                public Loader<List<LocationInfo>> onCreateLoader(int id, Bundle args) {
                    final String query = args.getString("query");
                    return new ResultsLoader(query, getActivity());
                }

                @Override
                public void onLoadFinished(Loader<List<LocationInfo>> listLoader,
                                           List<LocationInfo> locationSearchResults) {
                    mSearchResultsAdapter.changeArray(locationSearchResults);
                }

                @Override
                public void onLoaderReset(Loader<List<LocationInfo>> listLoader) {
                    mSearchResultsAdapter.changeArray(null);
                }
            };

    private class SearchResultsListAdapter extends BaseAdapter {
        private List<LocationInfo> mResults;

        private SearchResultsListAdapter() {
            mResults = new ArrayList<LocationInfo>();
        }

        public void changeArray(List<LocationInfo> results) {
            if (results == null) {
                results = new ArrayList<LocationInfo>();
            }

            mResults = results;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return Math.max(1, mResults.size());
        }

        @Override
        public Object getItem(int position) {
            if (position == 0 && mResults.size() == 0) {
                return null;
            }

            return mResults.get(position);
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup container) {
            if (convertView == null) {
                convertView = LayoutInflater.from(getActivity())
                        .inflate(R.layout.list_item_weather_location_result, container, false);
            }

            if (position == 0 && mResults.size() == 0) {
                ((TextView) convertView.findViewById(R.id.text1))
                        .setText(R.string.pref_weather_location_automatic);
                ((TextView) convertView.findViewById(R.id.text2))
                        .setText(R.string.pref_weather_location_automatic_description);
            } else {
                LocationInfo result = mResults.get(position);
                ((TextView) convertView.findViewById(R.id.text1))
                        .setText(result.getDisplayName());
                ((TextView) convertView.findViewById(R.id.text2))
                        .setText(result.getCountry());
            }

            return convertView;
        }
    }

    private static class ResultsLoader extends AsyncTaskLoader<List<LocationInfo>> {
        private String mQuery;
        private List<LocationInfo> mResults;

        public ResultsLoader(String query, Context context) {
            super(context);
            mQuery = query;
        }

        @Override
        public List<LocationInfo> loadInBackground() {
            return OpenWeatherMapApi.findLocationsAutocomplete(mQuery);
        }

        @Override
        public void deliverResult(List<LocationInfo> apps) {
            mResults = apps;

            if (isStarted()) {
                // If the Loader is currently started, we can immediately
                // deliver its results.
                super.deliverResult(apps);
            }
        }

        @Override
        protected void onStartLoading() {
            if (mResults != null) {
                deliverResult(mResults);
            }

            if (takeContentChanged() || mResults == null) {
                // If the data has changed since the last time it was loaded
                // or is not currently available, start a load.
                forceLoad();
            }
        }

        @Override
        protected void onStopLoading() {
            // Attempt to cancel the current load task if possible.
            cancelLoad();
        }

        @Override
        protected void onReset() {
            super.onReset();
            onStopLoading();
        }
    }
}
