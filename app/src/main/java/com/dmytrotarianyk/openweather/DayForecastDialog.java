package com.dmytrotarianyk.openweather;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.toolbox.NetworkImageView;
import com.dmytrotarianyk.openweather.api.ApiConstants;
import com.dmytrotarianyk.openweather.models.DayForecast;
import com.dmytrotarianyk.openweather.models.HourForecast;

public class DayForecastDialog {

    private Context context;
    private DayForecast forecast;
    private AlertDialog dialog;

    public DayForecastDialog(Context context, DayForecast forecast) {
        this.context = context;
        this.forecast = forecast;

        init();
    }

    protected void init() {
        View contentView = LayoutInflater.from(context).inflate(R.layout.day_forecast_dialog, null);
        dialog = new AlertDialog.Builder(context)
                .setView(contentView)
                .setPositiveButton(android.R.string.ok, null)
                .create();

        TextView dateTime = (TextView) contentView.findViewById(R.id.date_time);
        String day = Util.getWeekDay("EEEE", forecast.getDt());
        String time = Util.getDate(forecast.getDt());
        dateTime.setText(String.format("%1s, %2s", day, time));

        NetworkImageView icon = (NetworkImageView) contentView.findViewById(R.id.weather_icon);
        icon.setImageUrl(String.format(ApiConstants.WEATHER_ICON_URL, forecast.getIcon()),
                OpenWeatherApp.getImageLoader());

        TextView tempH = (TextView) contentView.findViewById(R.id.temp_h);
        tempH.setText(context.getString(R.string.temp_high_fmt, forecast.getTempH()) + "\u00B0" + "C");

        TextView tempL = (TextView) contentView.findViewById(R.id.temp_l);
        tempL.setText(context.getString(R.string.temp_low_fmt, forecast.getTempL()) + "\u00B0" + "C");

        TextView conditions = (TextView) contentView.findViewById(R.id.conditions);
        conditions.setText(forecast.getConditions());

        TextView humidity = (TextView) contentView.findViewById(R.id.humidity);
        humidity.setText(forecast.getHumidity() + "%");

        TextView windSpeed = (TextView) contentView.findViewById(R.id.wind_speed);
        String wind = String.format(context.getString(R.string.wind_fmt), forecast.getWindSpeed());
        windSpeed.setText(wind + " m/s");

        WindDirection direction = WindDirection.getDirection(forecast.getWindDeg());

        ImageView windIcon = (ImageView) contentView.findViewById(R.id.wind_icon);
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_wind_n);
        Bitmap bmResult = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bmResult);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setFilterBitmap(true);
        canvas.rotate(direction.getDegrees(), bitmap.getWidth() / 2, bitmap.getHeight() / 2);
        canvas.drawBitmap(bitmap, 0, 0, paint);
        windIcon.setImageBitmap(bmResult);

        TextView windDirection = (TextView) contentView.findViewById(R.id.wind_direction);
        windDirection.setText(direction.getCode());
    }

    public void show() {
        dialog.show();
    }
}
