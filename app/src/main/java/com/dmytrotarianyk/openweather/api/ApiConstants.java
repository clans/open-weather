package com.dmytrotarianyk.openweather.api;

public final class ApiConstants {

    private ApiConstants() {
    }

    public static final String APPID = "8214ce54696c9025fb5264abfbef7096";

    public static final String BASE_URL = "http://api.openweathermap.org/data/";
    public static final String API_VERSION = "2.5/";

    public static final String HOUR_FORECAST_URL = BASE_URL + API_VERSION + "forecast";
    public static final String DAY_FORECAST_URL = BASE_URL + API_VERSION + "forecast/daily";
    public static final String FIND_LOCATION_URL = BASE_URL + API_VERSION + "find";

    public static final String WEATHER_ICON_URL = "http://openweathermap.org/img/w/%s.png";
}
