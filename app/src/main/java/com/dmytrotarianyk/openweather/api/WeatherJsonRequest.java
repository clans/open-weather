package com.dmytrotarianyk.openweather.api;

import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.dmytrotarianyk.openweather.models.BaseModel;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;

public class WeatherJsonRequest<T extends BaseModel> extends Request<T> {

    private final Response.Listener<T> listener;
    private final Class<T> clazz;

    public WeatherJsonRequest(String url, Class<T> clazz, Response.Listener<T> listener,
                              Response.ErrorListener errorListener) {
        super(Method.GET, url, errorListener);

        this.listener = listener;
        this.clazz = clazz;
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString =
                    new String(response.data, HttpHeaderParser.parseCharset(response.headers));

            T parser = clazz.newInstance();
            return Response.success(clazz.cast(parser.parseJson(new JSONObject(jsonString))),
                    HttpHeaderParser.parseCacheHeaders(response));
        } catch (UnsupportedEncodingException e) {
            return Response.error(new ParseError(e));
        } catch (InstantiationException e) {
            return Response.error(new ParseError(e));
        } catch (IllegalAccessException e) {
            return Response.error(new ParseError(e));
        } catch (JSONException e) {
            return Response.error(new ParseError(e));
        }
    }

    @Override
    protected void deliverResponse(T response) {
        listener.onResponse(response);
    }
}
