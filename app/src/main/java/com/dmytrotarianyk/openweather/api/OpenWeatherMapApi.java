package com.dmytrotarianyk.openweather.api;

import android.util.Log;

import com.android.volley.Response;
import com.dmytrotarianyk.openweather.Util;
import com.dmytrotarianyk.openweather.models.DayForecastCollection;
import com.dmytrotarianyk.openweather.models.HourForecastCollection;
import com.dmytrotarianyk.openweather.models.LocationInfo;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class OpenWeatherMapApi {

    private static final String TAG = "OpenWeather";
    private static final int MAX_SEARCH_RESULTS = 10;

    private static String buildQueryString(LinkedHashMap<String, Object> parts) {
        String qs = "?";
        boolean first = true;

        ArrayList<String> keys = new ArrayList<String>(parts.keySet());

        for (String key : keys) {
            if (!first) {
                qs += "&";
            }
            if (parts.get(key) != null) {
                qs += key.toLowerCase() + "=" + parts.get(key).toString();
            }

            first = false;
        }

        return qs.equals("?") ? "" : qs;
    }

    public static WeatherJsonRequest getLocationInfo(final double lon, final double lat,
                                                     Response.Listener<LocationInfo> listener,
                                                     Response.ErrorListener errorListener) {

        LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>() {
            {
                put("lon", lon);
                put("lat", lat);
                put("mode", "json");
                put("units", "metric");
            }
        };

        String queryString = buildQueryString(map);
        String url = ApiConstants.FIND_LOCATION_URL + queryString + "&APPID=" + ApiConstants.APPID;
        return new WeatherJsonRequest<LocationInfo>(url, LocationInfo.class, listener, errorListener);
    }

    public static WeatherJsonRequest getHourlyForecast(final String locationName,
                                                       Response.Listener<HourForecastCollection> listener,
                                                       Response.ErrorListener errorListener) {

        LinkedHashMap<String, Object> map = new LinkedHashMap<String, Object>() {
            {
                put("q", locationName);
                put("mode", "json");
                put("units", "metric");
            }
        };

        String queryString = buildQueryString(map);
        String url = ApiConstants.HOUR_FORECAST_URL + queryString + "&APPID=" + ApiConstants.APPID;
        return new WeatherJsonRequest<HourForecastCollection>(url, HourForecastCollection.class,
                listener, errorListener);
    }

    public static WeatherJsonRequest getDailyForecast(final String locationName,
                                                      Response.Listener<DayForecastCollection> listener,
                                                      Response.ErrorListener errorListener) {

        LinkedHashMap<String, Object> parameterMap = new LinkedHashMap<String, Object>() {
            {
                put("q", locationName);
                put("mode", "json");
                put("units", "metric");
                put("cnt", "16"); // max value
            }
        };

        String queryString = buildQueryString(parameterMap);
        String url = ApiConstants.DAY_FORECAST_URL + queryString + "&APPID=" + ApiConstants.APPID;
        return new WeatherJsonRequest<DayForecastCollection>(url, DayForecastCollection.class,
                listener, errorListener);
    }

    public static List<LocationInfo> findLocationsAutocomplete(String startsWith) {
        List<LocationInfo> results = new ArrayList<LocationInfo>();

        HttpURLConnection connection = null;
        BufferedReader reader = null;
        try {
            StringBuilder sb = new StringBuilder();
            connection = Util.openUrlConnection(buildPlaceSearchStartsWithUrl(startsWith));
            reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));

            String line;
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }

            JSONObject jsonObject = new JSONObject(sb.toString());
            JSONArray jsonArray = jsonObject.getJSONArray("list");

            for (int i = 0; i < jsonArray.length(); i++) {
                results.add(new LocationInfo().parseJson(jsonArray.getJSONObject(i)));
            }
        } catch (IOException e) {
            Log.w(TAG, "Error parsing location JSON");
        } catch (JSONException e) {
            Log.w(TAG, "Error parsing location JSON");
        } finally {
            if (connection != null) {
                connection.disconnect();
            }

            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    Log.w(TAG, "Error parsing location JSON");
                }
            }
        }

        return results;
    }

    private static String buildPlaceSearchStartsWithUrl(String startsWith) {
        // GeoPlanet API
        startsWith = startsWith.replaceAll("[^\\w ]+", "").replaceAll(" ", "%20");
        return ApiConstants.FIND_LOCATION_URL + "?q=" + startsWith + "&type=like&mode=json&cnt="
                + MAX_SEARCH_RESULTS;
    }
}
