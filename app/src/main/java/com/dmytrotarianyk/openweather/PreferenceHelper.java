package com.dmytrotarianyk.openweather;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;

import com.dmytrotarianyk.openweather.models.LocationInfo;

public final class PreferenceHelper {

    private static final String KEY_LONGITUDE = "longitude";
    private static final String KEY_LATITUDE = "latitude";
    private static final String KEY_DISPLAY_NAME = "display_name";

    private static SharedPreferences mPrefs;

    private PreferenceHelper() {
    }

    public static void init(Context applicationContext) {
        mPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext);
    }

    public static void setLongitude(double lon) {
        Editor editor = mPrefs.edit();
        editor.putLong(KEY_LONGITUDE, Double.doubleToLongBits(lon));
        editor.apply();
    }

    public static void setLatitude(double lat) {
        Editor editor = mPrefs.edit();
        editor.putLong(KEY_LATITUDE, Double.doubleToLongBits(lat));
        editor.apply();
    }

    public static void setLocationName(String displayName) {
        Editor editor = mPrefs.edit();
        editor.putString(KEY_DISPLAY_NAME, displayName);
        editor.apply();
    }

    public static void clearSelectedLocation() {
        Editor editor = mPrefs.edit();
        editor.remove(KEY_LONGITUDE);
        editor.remove(KEY_LATITUDE);
        editor.remove(KEY_DISPLAY_NAME);
        editor.apply();
    }

    public static LocationInfo getLocationInfo() {
        LocationInfo location = new LocationInfo();
        location.setLongitude(Double.longBitsToDouble(mPrefs.getLong(KEY_LONGITUDE, 0)));
        location.setLatitude(Double.longBitsToDouble(mPrefs.getLong(KEY_LATITUDE, 0)));
        location.setDisplayName(mPrefs.getString(KEY_DISPLAY_NAME, ""));
        return location;
    }
}
