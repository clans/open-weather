package com.dmytrotarianyk.openweather;

import android.content.Context;
import android.text.format.DateUtils;
import android.util.DisplayMetrics;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Util {

    public static String capitalize(String description) {
        return description.substring(0, 1).toUpperCase() + description.substring(1).toLowerCase();
    }

    public static int getCacheSize(Context context) {
        final DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        final int screenWidth = displayMetrics.widthPixels;
        final int screenHeight = displayMetrics.heightPixels;
        final int screenBytes = screenWidth * screenHeight * 4; // 4 bytes per pixel

        return screenBytes * 3;
    }

    public static HttpURLConnection openUrlConnection(String url) throws IOException {
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setUseCaches(false);
        conn.setChunkedStreamingMode(0);
        conn.connect();
        return conn;
    }

    public static String getTime(Context context, long milliseconds) {
        return DateUtils.formatDateTime(context, milliseconds, DateUtils.FORMAT_SHOW_TIME);
    }

    public static String getWeekDay(String pattern, long milliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat(pattern);
        Date date = new Date(milliseconds);
        return sdf.format(date);
    }

    public static String getDate(long milliseconds) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM d");
        Date date = new Date(milliseconds);
        return sdf.format(date);
    }
}
