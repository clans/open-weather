package com.dmytrotarianyk.openweather;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.dmytrotarianyk.openweather.api.OpenWeatherMapApi;
import com.dmytrotarianyk.openweather.fragments.DailyWeatherFragment;
import com.dmytrotarianyk.openweather.fragments.HourlyWeatherFragment;
import com.dmytrotarianyk.openweather.fragments.LocationsFragment;
import com.dmytrotarianyk.openweather.models.LocationInfo;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.location.LocationClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.viewpagerindicator.TabPageIndicator;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

    private static final String TAG = "OpenWeather";
    private static final int CONNECTION_FAILURE_RESOLUTION_REQUEST = 300;
    private static final String KEY_SELECTED_PAGE = "selected_page";

    private ActionBar mActionBar;
    private ViewPager mViewPager;
    private LocationClient mLocationClient;
    private Location mLocation;
    private boolean mErrorDialogShown;
    private int mCurrentPage;
    private HourlyWeatherFragment mHourlyWeatherFragment;
    private DailyWeatherFragment mDailyWeatherFragment;
    private LocationInfo mLocationInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        supportRequestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        setContentView(R.layout.main_activity);

        if (savedInstanceState != null) {
            mCurrentPage = savedInstanceState.getInt(KEY_SELECTED_PAGE, 0);
        }

        mActionBar = getSupportActionBar();

        mLocationClient = new LocationClient(this, mConnectionCallbacks, mConnectionFailedListener);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        mCurrentPage = savedInstanceState.getInt(KEY_SELECTED_PAGE, 0);

        mHourlyWeatherFragment = (HourlyWeatherFragment)
                getSupportFragmentManager().getFragment(savedInstanceState, "hourly");
        mDailyWeatherFragment = (DailyWeatherFragment)
                getSupportFragmentManager().getFragment(savedInstanceState, "daily");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mViewPager != null) {
            outState.putInt(KEY_SELECTED_PAGE, mViewPager.getCurrentItem());
        }

        if (mHourlyWeatherFragment != null) {
            getSupportFragmentManager().putFragment(outState, "hourly", mHourlyWeatherFragment);
        }

        if (mDailyWeatherFragment != null) {
            getSupportFragmentManager().putFragment(outState, "daily", mDailyWeatherFragment);
        }
    }

    public void updateTitle(String title) {
        mActionBar.setTitle(title);
    }

    private void initViewPager() {
        if (!haveInternet()) {
            Log.d(TAG, "No network connection; not attempting to update weather.");
            showNoConnection();
            return;
        }

        if (mHourlyWeatherFragment == null) {
            mHourlyWeatherFragment = HourlyWeatherFragment.newInstance(mLocationInfo);
        }

        if (mDailyWeatherFragment == null) {
            mDailyWeatherFragment = DailyWeatherFragment.newInstance(mLocationInfo);
        }

        List<Fragment> fragments = new ArrayList<Fragment>();
        fragments.add(mHourlyWeatherFragment);
        fragments.add(mDailyWeatherFragment);

        WeatherPagerAdapter mPagerAdapter = new WeatherPagerAdapter(getSupportFragmentManager(), fragments);

        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setPageMargin(getResources().getDimensionPixelSize(R.dimen.viewpager_margin));
        mViewPager.setAdapter(mPagerAdapter);

        TabPageIndicator mTabIndicator = (TabPageIndicator) findViewById(R.id.tabs);
        mTabIndicator.setViewPager(mViewPager);
        mTabIndicator.setVisibility(View.VISIBLE);
        mTabIndicator.setCurrentItem(mCurrentPage);

        hideLoadingIndicator();
    }

    public void updateLocation() {
        mLocationInfo = PreferenceHelper.getLocationInfo();
        mHourlyWeatherFragment = null;
        mDailyWeatherFragment = null;

        if (mLocationInfo.isNull()) {
            LocationRequest request = LocationRequest.create();
            request.setNumUpdates(1);

            mLocationClient.requestLocationUpdates(request, new LocationListener() {
                @Override
                public void onLocationChanged(Location location) {
                    mLocation = location;
                    getLocationInfo();
                }
            });
        } else {
            initViewPager();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.action_refresh);
        return true;
    }

    private boolean haveInternet() {
        NetworkInfo ni = ((ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
        return ni != null && ni.isConnected();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (!haveInternet()) return false;

        switch (item.getItemId()) {
            case R.id.action_refresh:
                if (mLocationInfo.isNull() || mViewPager == null) {
                    updateLocation();
                    return true;
                }

                switch (mViewPager.getCurrentItem()) {
                    case 0:
                        if (mHourlyWeatherFragment != null) {
                            mHourlyWeatherFragment.showRefreshConfirmDialog();
                        }
                        break;
                    case 1:
                        if (mDailyWeatherFragment != null) {
                            mDailyWeatherFragment.showRefreshConfirmDialog();
                        }
                        break;
                }
                return true;
            case R.id.action_location:
                showLocationsDialog();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showLocationsDialog() {
        getSupportFragmentManager().beginTransaction()
                .add(new LocationsFragment(), "locations_fragment").commit();
    }

    @Override
    public void onStart() {
        super.onStart();
        // Connect the client.
        mLocationClient.connect();
    }

    @Override
    public void onStop() {
        // Disconnecting the client invalidates it.
        mLocationClient.disconnect();
        super.onStop();
    }

    private void hideLoadingIndicator() {
        findViewById(R.id.content).setVisibility(View.VISIBLE);
        findViewById(R.id.loading_indicator).setVisibility(View.GONE);
        findViewById(R.id.no_connection).setVisibility(View.GONE);
    }

    private void showNoConnection() {
        findViewById(R.id.no_connection).setVisibility(View.VISIBLE);
        findViewById(R.id.loading_indicator).setVisibility(View.GONE);
        findViewById(R.id.content).setVisibility(View.GONE);
    }

    private void getLocationInfo() {
        OpenWeatherApp.getRequestQueue().add(OpenWeatherMapApi.getLocationInfo(
                mLocation.getLongitude(), mLocation.getLatitude(),
                new Response.Listener<LocationInfo>() {
                    @Override
                    public void onResponse(LocationInfo response) {
                        mLocationInfo = response;
                        initViewPager();
                    }
                }, new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        if (error.getCause() instanceof UnknownHostException) {
                            showNoConnection();
                        }
                        // TODO: handle errors
                    }
                }));
    }

    GooglePlayServicesClient.ConnectionCallbacks mConnectionCallbacks
            = new GooglePlayServicesClient.ConnectionCallbacks() {

        @Override
        public void onConnected(Bundle bundle) {
            mLocation = mLocationClient.getLastLocation();
            mLocationInfo = PreferenceHelper.getLocationInfo();

            // sometimes location isn't correct, have to make a search call
            if (mLocationInfo.isNull()) {
                if (mLocation == null) {
                    updateLocation();
                } else {
                    getLocationInfo();
                }
            } else {
                initViewPager();
            }
        }

        @Override
        public void onDisconnected() {
        }
    };

    GooglePlayServicesClient.OnConnectionFailedListener mConnectionFailedListener
            = new GooglePlayServicesClient.OnConnectionFailedListener() {

        @Override
        public void onConnectionFailed(ConnectionResult connectionResult) {
            if (connectionResult.hasResolution()) {
                try {
                    // Start an Activity that tries to resolve the error
                    connectionResult.startResolutionForResult(MainActivity.this,
                            CONNECTION_FAILURE_RESOLUTION_REQUEST);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            } else if (connectionResult.getErrorCode() >= 0 && !mErrorDialogShown) {
                mErrorDialogShown = true;
                GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(),
                        MainActivity.this, CONNECTION_FAILURE_RESOLUTION_REQUEST).show();
            } else {
                showLocationsDialog();
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CONNECTION_FAILURE_RESOLUTION_REQUEST) {
            if (resultCode != Activity.RESULT_OK) {
                // TODO: show search dialog
            }
        }
    }

    private class WeatherPagerAdapter extends FragmentStatePagerAdapter {

        List<Fragment> fragments;

        public WeatherPagerAdapter(FragmentManager fm, List<Fragment> fragments) {
            super(fm);
            this.fragments = fragments;
        }

        @Override
        public Fragment getItem(int i) {
            return fragments.get(i);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return getString(R.string.hourly);
                case 1:
                    return getString(R.string.daily);
            }
            return super.getPageTitle(position);
        }

        @Override
        public int getCount() {
            return 2;
        }
    }
}
