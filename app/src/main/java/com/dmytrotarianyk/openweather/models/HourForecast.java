package com.dmytrotarianyk.openweather.models;

import com.dmytrotarianyk.openweather.Util;

import org.json.JSONArray;
import org.json.JSONObject;

public class HourForecast {

    private long dt;
    private int temp;
    private int pressure;
    private int humidity;

    private int weatherCode;
    private String title;
    private String conditions;
    private String icon;

    private double windSpeed;
    private int windDeg;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

    public int getPressure() {
        return pressure;
    }

    public void setPressure(int pressure) {
        this.pressure = pressure;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public int getWindDeg() {
        return windDeg;
    }

    public void setWindDeg(int windDeg) {
        this.windDeg = windDeg;
    }

    public HourForecast parseJson(JSONObject jsonObject) {
        setDt(Long.parseLong(jsonObject.optString("dt") + "000"));

        JSONObject mainJson = jsonObject.optJSONObject("main");
        setTemp(mainJson.optInt("temp"));
        setPressure(mainJson.optInt("pressure"));
        setHumidity(mainJson.optInt("humidity"));

        JSONArray weatherArray = jsonObject.optJSONArray("weather");
        JSONObject weatherObject = weatherArray.optJSONObject(0);
        setWeatherCode(weatherObject.optInt("id"));
        setTitle(weatherObject.optString("main"));
        setConditions(Util.capitalize(weatherObject.optString("description")));
        setIcon(weatherObject.optString("icon"));

        JSONObject windJson = jsonObject.optJSONObject("wind");
        if (windJson != null) {
            setWindSpeed(windJson.optDouble("speed"));
            setWindDeg(windJson.optInt("deg"));
        }

        return this;
    }
}
