package com.dmytrotarianyk.openweather.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class HourForecastCollection extends ArrayList<HourForecast> implements BaseModel<HourForecastCollection> {

    private LocationInfo locationInfo;

    public LocationInfo getLocationInfo() {
        return locationInfo;
    }

    @Override
    public HourForecastCollection parseJson(JSONObject jsonObject) {
        JSONObject jsonCity = jsonObject.optJSONObject("city");
        locationInfo = new LocationInfo().parseJson(jsonCity);

        JSONArray jsonArray = jsonObject.optJSONArray("list");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonHour = jsonArray.optJSONObject(i);
            HourForecast hourForecast = new HourForecast();
            this.add(hourForecast.parseJson(jsonHour));
        }
        return this;
    }
}
