package com.dmytrotarianyk.openweather.models;

import org.json.JSONObject;

public interface BaseModel<T> {

    T parseJson(JSONObject jsonObject);
}
