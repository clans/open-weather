package com.dmytrotarianyk.openweather.models;

import android.text.TextUtils;

import org.json.JSONArray;
import org.json.JSONObject;

public class LocationInfo implements BaseModel {

    private String id;
    private String name;
    private String country;
    private String displayName;
    private double latitude;
    private double longitude;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public boolean isNull() {
        return TextUtils.isEmpty(displayName);
    }

    @Override
    public LocationInfo parseJson(JSONObject jsonObject) {
        if (jsonObject.has("list")) {
            JSONArray jsonArray = jsonObject.optJSONArray("list");
            jsonObject = jsonArray.optJSONObject(0);
        }

        id = jsonObject.optString("id");
        StringBuilder sb = new StringBuilder();
        name = jsonObject.optString("name");
        country = jsonObject.optString("country");

        if (TextUtils.isEmpty(country) && jsonObject.has("sys")) {
            country = jsonObject.optJSONObject("sys").optString("country");
        }

        if (!TextUtils.isEmpty(name)) {
            sb.append(name);
        }

        if (!TextUtils.isEmpty(country)) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(country);
        }

        displayName = sb.toString();

        JSONObject coordinates = jsonObject.optJSONObject("coord");
        longitude = Double.parseDouble(coordinates.optString("lon"));
        latitude = Double.parseDouble(coordinates.optString("lat"));

        return this;
    }
}
