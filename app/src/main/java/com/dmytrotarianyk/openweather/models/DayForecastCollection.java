package com.dmytrotarianyk.openweather.models;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DayForecastCollection extends ArrayList<DayForecast> implements BaseModel<DayForecastCollection> {

    @Override
    public DayForecastCollection parseJson(JSONObject jsonObject) {
        JSONArray jsonArray = jsonObject.optJSONArray("list");
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonDay = jsonArray.optJSONObject(i);
            DayForecast dayForecast = new DayForecast();
            this.add(dayForecast.parseJson(jsonDay));
        }
        return this;
    }
}
