package com.dmytrotarianyk.openweather.models;

import com.dmytrotarianyk.openweather.Util;

import org.json.JSONArray;
import org.json.JSONObject;

public class DayForecast {

    private long dt;
    private int tempH;
    private int tempL;
    private int humidity;

    private int weatherCode;
    private String title;
    private String conditions;
    private String icon;

    private double windSpeed;
    private int windDeg;

    public long getDt() {
        return dt;
    }

    public void setDt(long dt) {
        this.dt = dt;
    }

    public int getTempH() {
        return tempH;
    }

    public void setTempH(int tempH) {
        this.tempH = tempH;
    }

    public int getTempL() {
        return tempL;
    }

    public void setTempL(int tempL) {
        this.tempL = tempL;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public int getWeatherCode() {
        return weatherCode;
    }

    public void setWeatherCode(int weatherCode) {
        this.weatherCode = weatherCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getConditions() {
        return conditions;
    }

    public void setConditions(String conditions) {
        this.conditions = conditions;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public int getWindDeg() {
        return windDeg;
    }

    public void setWindDeg(int windDeg) {
        this.windDeg = windDeg;
    }

    public DayForecast parseJson(JSONObject jsonObject) {
        setDt(Long.parseLong(jsonObject.optString("dt") + "000"));
        JSONObject jsonTemp = jsonObject.optJSONObject("temp");
        setTempH(jsonTemp.optInt("max"));
        setTempL(jsonTemp.optInt("min"));
        setHumidity(jsonObject.optInt("humidity"));

        JSONArray weatherArray = jsonObject.optJSONArray("weather");
        JSONObject weatherObject = weatherArray.optJSONObject(0);
        setWeatherCode(weatherObject.optInt("id"));
        setTitle(weatherObject.optString("main"));
        setConditions(Util.capitalize(weatherObject.optString("description")));
        setIcon(weatherObject.optString("icon"));

        setWindSpeed(jsonObject.optDouble("speed"));
        setWindDeg(jsonObject.optInt("deg"));

        return this;
    }
}
