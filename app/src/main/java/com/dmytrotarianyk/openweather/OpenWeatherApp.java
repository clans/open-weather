package com.dmytrotarianyk.openweather;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

public class OpenWeatherApp extends Application {

    private static RequestQueue sRequestQueue;
    private static ImageLoader sImageLoader;

    public static RequestQueue getRequestQueue() {
        return sRequestQueue;
    }

    public static ImageLoader getImageLoader() {
        return sImageLoader;
    }

    @Override
    public void onCreate() {
        super.onCreate();

        sRequestQueue = Volley.newRequestQueue(this);
        sImageLoader = new ImageLoader(sRequestQueue, new LruBitmapCache(Util.getCacheSize(this)));

        PreferenceHelper.init(this);
    }
}
